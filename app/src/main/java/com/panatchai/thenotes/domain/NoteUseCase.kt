package com.panatchai.thenotes.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.panatchai.thenotes.data.models.Resource
import com.panatchai.thenotes.data.models.Status
import com.panatchai.thenotes.data.models.notes.RawNote
import com.panatchai.thenotes.data.repositories.NoteRepository
import com.panatchai.thenotes.ui.models.maps.MapLocation
import com.panatchai.thenotes.ui.models.notes.Note
import com.panatchai.thenotes.utils.AppExecutors
import com.panatchai.thenotes.utils.TimeUtil
import javax.inject.Inject

/**
 * Handling Note Related tasks.
 *
 * Note:
 * Usually, I would have a base case for UseCase, which always execute tasks
 * in a background thread but this is a simple app so... simple implementation.
 */
class NoteUseCase @Inject constructor(
        private val noteRepository: NoteRepository,
        private val userUseCase: UserUseCase,
        private val timeUtil: TimeUtil,
        private val appExecutors: AppExecutors
) {
    /**
     * Retrieve notes from the repository.
     *
     * @return list of notes and loading status.
     */
    fun getNotes(): LiveData<Resource<List<Note>>> = Transformations.switchMap(noteRepository.getNotes()) { resource ->
        val transformList = MutableLiveData<Resource<List<Note>>>()
        val resultList = resource.data?.map() ?: listOf()
        val msg = resource.message
        transformList.value = when (resource.status) {
            Status.SUCCESS -> Resource.success(msg, resultList)
            Status.LOADING -> Resource.loading(msg, resultList)
            Status.ERROR -> Resource.error(msg, resultList)
        }
        transformList
    }

    /**
     * Add new note to the repository
     *
     * @param note user's short note
     * @param location user's latitude and longitude where the note is created.
     */
    fun addNote(note: String, location: MapLocation) {
        val rawNote = RawNote(
                uid = userUseCase.getCurrentUserId(),
                uname = userUseCase.getCurrentUserName(),
                lat = location.latitude,
                lng = location.longitude,
                created = timeUtil.getCurrentTimeLong(),
                modified = 0,
                note = note
        )
        noteRepository.addNote(rawNote)
    }

    /**
     * Check whether or not the note can be edited.
     * Only owner of the note can edit.
     *
     * @param note note to be edited
     */
    fun canEditNote(note: Note): Boolean {
        return userUseCase.getCurrentUserId() == note.userId
    }

    /**
     * Update existing note in the repository. Only owner can edit it though.
     *
     * @param note note to be updated
     */
    fun updateNote(note: Note) {
        if (userUseCase.getCurrentUserId() == note.userId) {
            noteRepository.updateNote(note.map())
        }
    }

    /**
     * Query if any notes contain [query] string.
     */
    fun queryNote(query: String): LiveData<Resource<List<Note>>> {
        val regex = ".*${query.toLowerCase()}.*".toRegex()
        val result = MutableLiveData<Resource<List<Note>>>()
        val liveNote = noteRepository.getNotes()
        val observer = object : Observer<Resource<List<RawNote>>> {
            override fun onChanged(resource: Resource<List<RawNote>>?) {
                when (resource?.status) {
                    Status.SUCCESS -> {
                        appExecutors.networkIO().execute {
                            val filterList = resource.data?.filter { rawNote ->
                                rawNote.uname.toLowerCase().contains(regex) or
                                        rawNote.note.toLowerCase().contains(regex)
                            }?.map { rawNote ->
                                rawNote.map()
                            }

                            if (filterList != null && filterList.isNotEmpty()) {
                                result.postValue(Resource.success(filterList))
                            } else {
                                result.postValue(Resource.error("$query Not Found!"))
                            }
                        } // end execute
                        liveNote.removeObserver(this)
                    }
                    Status.ERROR -> {
                        result.value = Resource.error(resource.message)
                        liveNote.removeObserver(this)
                    }
                    Status.LOADING -> {
                        result.value = Resource.loading(null)
                    }
                } // end When
            } // end onChanged
        } // end observer
        liveNote.observeForever(observer)
        return result
    }
}