package com.panatchai.thenotes.domain

import com.panatchai.thenotes.data.models.notes.RawNote
import com.panatchai.thenotes.ui.models.notes.Note

fun RawNote.map(): Note = Note(
    userId = uid,
    userName = uname,
    latitude = lat,
    longitude = lng,
    createdDate = created,
    modifiedDate = modified,
    note = note
)

fun Note.map(): RawNote = RawNote(
    uid = userId,
    uname = userName,
    lat = latitude,
    lng = longitude,
    created = createdDate,
    modified = modifiedDate,
    note = note
)

fun List<RawNote>.map(): List<Note> {
    val list = mutableListOf<Note>()
    for (rawNote in this) {
        list.add(rawNote.map())
    }
    return list
}