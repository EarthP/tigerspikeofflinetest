package com.panatchai.thenotes.domain

import com.panatchai.thenotes.data.repositories.UserSharedPrefs
import java.util.*
import javax.inject.Inject

/**
 * Accessing User ID. If the user exists, return the current user.
 * Otherwise, create a new user, save, and return the user.
 *
 * Note:
 * Usually, I would have a base case for UseCase, which always execute tasks
 * in a background thread but this is a simple app so... simple implementation.
 */
class UserUseCase @Inject constructor(
        private val userSharedPrefs: UserSharedPrefs
) {
    fun getCurrentUserId(): Long {
        var currentUserId = userSharedPrefs.getUserId()
        if (currentUserId == 0L) {
            currentUserId = Calendar.getInstance().timeInMillis
            userSharedPrefs.saveUserId(currentUserId)
        }
        return currentUserId
    }

    fun getCurrentUserName(): String {
        var currentUserName = userSharedPrefs.getUserName()
        if (currentUserName.isEmpty()) {
            val charPool = arrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0')
            currentUserName = (1..10)
                    .map { kotlin.random.Random.nextInt(0, charPool.size) }
                    .map(charPool::get)
                    .joinToString("")
                    .capitalize()
            userSharedPrefs.saveUseName(currentUserName)
        }
        return currentUserName
    }
}