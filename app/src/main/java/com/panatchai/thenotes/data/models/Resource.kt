package com.panatchai.thenotes.data.models

import com.panatchai.thenotes.data.models.Status.*

/**
 * A generic class that holds a value with its loading status.
 *
 * @param <T> resource type.
</T> */
data class Resource<out T>(val status: Status, val data: T?, val message: String = "") {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, "")
        }

        fun <T> success(msg: String, data: T?): Resource<T> {
            return Resource(SUCCESS, data, msg)
        }

        fun <T> error(data: T? = null): Resource<T> {
            return Resource(ERROR, data, "")
        }

        fun <T> error(msg: String, data: T? = null): Resource<T> {
            return Resource(ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LOADING, data, "")
        }

        fun <T> loading(msg: String, data: T?): Resource<T> {
            return Resource(LOADING, data, msg)
        }
    }
}
