package com.panatchai.thenotes.data.repositories

import android.content.Context
import com.panatchai.thenotes.di.scopes.ForApplication
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Managing user related functions using SharedPrefs.
 */
@Singleton
class UserSharedPrefs @Inject constructor(
        @ForApplication private val appContext: Context
) : SharedPrefs(appContext) {

    /**
     * Get Current User ID.
     *
     * @return user id
     */
    fun getUserId() = sharedPrefs.getLong(USER_ID, 0)

    /**
     * Save User ID.
     *
     * @param userId user id
     */
    fun saveUserId(userId: Long) {
        sharedPrefs.edit()
                .putLong(USER_ID, userId)
                .apply()
    }

    fun getUserName(): String = sharedPrefs.getString(USER_NAME, "") ?: ""

    fun saveUseName(userName: String) {
        sharedPrefs.edit()
                .putString(USER_NAME, userName)
                .apply()
    }

    companion object {
        internal const val USER_ID = "USER_ID"
        internal const val USER_NAME = "USER_NAME"
    }
}