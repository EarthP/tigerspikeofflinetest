package com.panatchai.thenotes.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.panatchai.thenotes.data.models.Resource
import com.panatchai.thenotes.data.models.Status
import com.panatchai.thenotes.data.models.notes.RawNote
import com.panatchai.thenotes.utils.AppExecutors
import com.panatchai.thenotes.utils.LogUtils
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Store and retrieve [RawNote].
 */
abstract class NoteRepository {
    /**
     * Get all notes store in the database.
     *
     * @return list of notes
     */
    abstract fun getNotes(): LiveData<Resource<List<RawNote>>>

    /**
     * Save a new note into the database.
     *
     * @param note note to be saved.
     */
    abstract fun addNote(note: RawNote)

    /**
     * Save an existing note into the database.
     *
     * @param note note to be saved.
     */
    abstract fun updateNote(note: RawNote)
}

@Singleton
class NoteRepositoryImp @Inject constructor(
        private val appExecutors: AppExecutors
) : NoteRepository() {

    // Firebase DB
    private val database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val dataRef: DatabaseReference

    // data read from Firebase
    private val noteList = MutableLiveData<Resource<List<RawNote>>>()

    // data changed listener
    private val listener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            appExecutors.networkIO().execute {
                // This method is called once with the initial value and again
                // whenever requestCode at this location is updated.
                val notes = mutableListOf<RawNote>()
                dataSnapshot.children.mapNotNullTo(notes) { snapshot ->
                    snapshot.getValue(RawNote::class.java)
                }
                noteList.postValue(Resource.success(notes))
            }
        }

        override fun onCancelled(error: DatabaseError) {
            // Failed to read value
            LogUtils.e("NoteRepositoryImp", "Failed to read value.", error.toException())
            noteList.value = Resource.error(error.message)
        }
    }

    init {
        // allow storing data offline
        database.setPersistenceEnabled(true)

        // init root db
        dataRef = database.getReference(ROOT_DB_NAME)
    }

    // Read from the database
    override fun getNotes(): LiveData<Resource<List<RawNote>>> {
        when {
            noteList.value == null -> { // First time calling this method
                noteList.value = Resource.loading(null)
                dataRef.addValueEventListener(listener)
            }
            noteList.value?.status != Status.LOADING -> { // second time calling this method
                noteList.value = Resource.loading(null)
                dataRef.removeEventListener(listener)
                dataRef.addValueEventListener(listener)
            }
        }
        return noteList
    }

    // Save to the database
    override fun addNote(note: RawNote) {
        updateNote(note)
    }

    // Save to the database
    override fun updateNote(note: RawNote) {
        val key = note.created.toString()
        dataRef.child(key).setValue(note)
    }

    companion object {
        private const val ROOT_DB_NAME = "notes"
    }
}