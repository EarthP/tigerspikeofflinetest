package com.panatchai.thenotes.data.models.notes

data class RawNote(
    val uid: Long = 0,
    val uname: String = "",
    val lat: Double = 0.0,
    val lng: Double = 0.0,
    val created: Long = 0,
    val modified: Long = 0,
    val note: String = ""
) {
    override fun toString(): String {
        return "userId: $uid, uname: $uname, latitude: $lat, longitude: $lng, createdDate: $created, modifiedDate: $modified, note: $note"
    }
}