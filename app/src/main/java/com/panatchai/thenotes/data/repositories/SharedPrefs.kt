package com.panatchai.thenotes.data.repositories

import android.content.Context
import com.panatchai.thenotes.BuildConfig

/**
 * Facilitate accessing Android SharedPrefs.
 */
abstract class SharedPrefs constructor(
    appContext: Context
) {
    protected val sharedPrefs = appContext.getSharedPreferences(
        SHARED_PREFERENCES_NAME,
        Context.MODE_PRIVATE
    )

    companion object {
        internal val SHARED_PREFERENCES_NAME = "${BuildConfig.APPLICATION_ID}.${BuildConfig.DEBUG}"
    }
}