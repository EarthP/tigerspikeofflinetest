package com.panatchai.thenotes.data.models

/**
 * Status of a resource that is provided to the UI.
 *
 * These are usually createdDate by the Repository classes where they return
 * `LiveData<Resource<T>>` to pass back the latest requestCode to the UI with its fetch status.
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
