package com.panatchai.thenotes.extensions

import com.panatchai.thenotes.ui.models.notes.Note
import java.util.*

fun Note.updateNote(newNote: String): Note {
    val modifiedTime = Calendar.getInstance().timeInMillis
    return Note(
        userId = userId,
        userName = userName,
        latitude = latitude,
        longitude = longitude,
        createdDate = createdDate,
        modifiedDate = modifiedTime,
        note = newNote
    )
}