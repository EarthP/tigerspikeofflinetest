package com.panatchai.thenotes.utils

import android.app.Application
import java.util.*
import javax.inject.Inject

class TimeUtil @Inject constructor(
    private val app: Application
) {
    fun getCurrentTimeLong(): Long {
        val locale = app.resources.configuration.locales.get(0)
        val calendar = Calendar.getInstance(locale)
        return calendar.timeInMillis
    }
}