package com.panatchai.thenotes.utils

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.panatchai.thenotes.ui.models.maps.PermissionRequest

/**
 * Base Class for requesting any Permissions.
 */
abstract class BasePermissionHelper {
    private val permissionStatus = MediatorLiveData<PermissionRequest>()
    private val requestingPermissions = mutableMapOf<Int, MutableLiveData<PermissionRequest>>()

    fun granted(requestCode: Int) {
        requestingPermissions[requestCode]?.let { status ->
            permissionStatus.value = PermissionRequest.granted(requestCode)
            permissionStatus.removeSource(status)
            requestingPermissions.remove(requestCode)
        }
    }

    fun denied(requestCode: Int) {
        requestingPermissions[requestCode]?.let { status ->
            permissionStatus.value = PermissionRequest.denied(requestCode)
            permissionStatus.removeSource(status)
            requestingPermissions.remove(requestCode)
        }
    }

    fun getRequestingPermissionStatus(): LiveData<PermissionRequest> = permissionStatus

    fun requestPermission(activity: Activity, request: PermissionRequest) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(request.permission),
            request.requestCode
        )
    }

    fun checkPermission(
        activity: Activity,
        requestCode: Int,
        permission: String
    ): LiveData<PermissionRequest> {

        // if the same permission request is being requested again, ignore it.
        if (requestingPermissions[requestCode] != null) {
            return permissionStatus
        }

        val status = MutableLiveData<PermissionRequest>()
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            requestingPermissions[requestCode] = status
            permissionStatus.addSource(status) { permissionRequest ->
                permissionStatus.value = permissionRequest
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                status.value = PermissionRequest.rationale(requestCode, permission)
            } else {
                requestPermission(activity, PermissionRequest.request(requestCode, permission))
            }
        } else {
            permissionStatus.value = PermissionRequest.granted(requestCode)
        }
        return permissionStatus
    }
}