package com.panatchai.thenotes.utils

import android.content.Context
import com.panatchai.thenotes.di.scopes.PerActivity
import javax.inject.Inject

/**
 * Provide Resources (e.g. String, Integer, etc..)
 *
 * Note: In this case we only have string.
 */
@PerActivity
open class ResourceProvider @Inject constructor(
        private val context: Context
) {
    fun getString(resId: Int): String {
        return context.getString(resId)
    }

    fun getString(resId: Int, value: String): String {
        return context.getString(resId, value)
    }
}