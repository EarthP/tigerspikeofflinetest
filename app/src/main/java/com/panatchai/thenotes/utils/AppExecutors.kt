package com.panatchai.thenotes.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
@Singleton
open class AppExecutors(
    private val diskIO: Executor,
    private val networkIO: Executor,
    private val mainThread: Executor
) {

    @Inject
    constructor() : this(
        Executors.newSingleThreadExecutor(),
        Executors.newFixedThreadPool(POOL_SIZE),
        MainThreadExecutor()
    )

    /**
     * This will execute in FIFO fashion.
     */
    fun diskIO(): Executor {
        return diskIO
    }

    /**
     * This will execute tasks in parallel.
     */
    fun networkIO(): Executor {
        return networkIO
    }

    /**
     * This will execute on the Main thread.
     */
    fun mainThread(): Executor {
        return mainThread
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }

    companion object {
        private const val POOL_SIZE = 3
    }
}
