package com.panatchai.thenotes.utils

import android.util.Log
import com.panatchai.thenotes.BuildConfig
import java.io.PrintWriter
import java.io.StringWriter

object LogUtils {

    fun e(tag: String?, message: String?, vararg args: Any) {
        if (tag == null || message == null || !BuildConfig.DEBUG) {
            return
        }

        if (args.isNotEmpty()) {
            Log.e(tag, String.format(message, *args))
        } else {
            Log.e(tag, message)
        }
    }

    fun e(tag: String, t: Throwable) {
        if (!BuildConfig.DEBUG) {
            return
        }

        val sw = StringWriter(256)
        val pw = PrintWriter(sw, false)
        t.printStackTrace(pw)
        pw.flush()
        val message = sw.toString()
        Log.e(tag, message)
    }
}