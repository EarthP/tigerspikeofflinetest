package com.panatchai.thenotes.ui.maps

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.panatchai.thenotes.data.models.Resource
import com.panatchai.thenotes.data.models.Status
import com.panatchai.thenotes.domain.NoteUseCase
import com.panatchai.thenotes.ui.models.maps.MapLocation
import com.panatchai.thenotes.ui.models.notes.Note
import com.panatchai.thenotes.utils.AbsentLiveData
import javax.inject.Inject

/**
 * Orchestrate Maps View.
 */
class MapsViewModel @Inject constructor(
        private val noteUseCase: NoteUseCase,
        private val resourceProvider: MapsResourceProvider
) : ViewModel() {

    private var _queryText = ""
    private var _userLastKnownLocation: MapLocation? = null
    private lateinit var noteList: LiveData<Resource<List<Note>>>

    val userLocation: LiveData<MapLocation> = MutableLiveData<MapLocation>()
    val isAddNoteAllowed: LiveData<Boolean> = MutableLiveData<Boolean>()

    private val _isMapReady = MutableLiveData<Boolean>()
    val isMapReady: LiveData<Resource<List<Note>>> = Transformations.switchMap(_isMapReady) { isReady ->
        if (!isReady) {
            AbsentLiveData.create()
        } else {
            noteList = if (_queryText.isEmpty()) {
                noteUseCase.getNotes()
            } else {
                noteUseCase.queryNote(_queryText)
            }
            noteList
        }
    }

    fun getCurrentNoteList(): LiveData<Resource<List<Note>>> {
        return noteList
    }

    fun setMapReady(isReady: Boolean) {
        _isMapReady.value = isReady
    }

    fun updateUserLocation(resource: Resource<MapLocation>) {
        _userLastKnownLocation = resource.data

        when (resource.status) {
            Status.SUCCESS -> {
                _userLastKnownLocation?.let { location ->
                    (userLocation as MutableLiveData).value = location
                    (isAddNoteAllowed as MutableLiveData).value = true
                }
            }
            else -> {
                (isAddNoteAllowed as MutableLiveData).value = false
            }
        }
    }

    fun addNote(note: String) {
        if (note.isNotEmpty()) {
            _userLastKnownLocation?.let { location ->
                noteUseCase.addNote(note, location)
            }
        }
    }

    fun editNote(noteId: String): LiveData<Resource<Note>> {
        val editNote = MutableLiveData<Resource<Note>>()
        val resource = noteList.value
        if (resource?.data != null) {
            val editingNote = resource.data.singleOrNull { note ->
                note.id == noteId
            }
            if (editingNote != null) {
                if (noteUseCase.canEditNote(editingNote)) {
                    editNote.value = Resource.success(editingNote)
                } else {
                    editNote.value = Resource.error(resourceProvider.cannotEditNoteError)
                }
            } else {
                editNote.value = Resource.error(resourceProvider.noteNotFound)
            }
        } else {
            editNote.value = Resource.error(resourceProvider.noteNotFound)
        }
        return editNote
    }

    fun updateNote(note: Note) {
        noteUseCase.updateNote(note)
    }

    fun queryText(query: String) {
        _queryText = query
        _isMapReady.value = _isMapReady.value ?: false
    }
}