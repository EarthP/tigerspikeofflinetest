package com.panatchai.thenotes.ui.maps

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.panatchai.thenotes.R
import com.panatchai.thenotes.databinding.LayoutNoteInfoBinding
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.common.DataBoundListAdapter
import com.panatchai.thenotes.ui.models.notes.Note
import com.panatchai.thenotes.utils.AppExecutors
import javax.inject.Inject

@PerActivity
class NoteListAdapter @Inject constructor(
        private val dataBindingComponent: NoteDataBindingComponent,
        appExecutors: AppExecutors,
        callback: NoteItemDiffCallback
) : DataBoundListAdapter<Note, ViewDataBinding>(
        appExecutors = appExecutors,
        diffCallback = callback
) {
    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val inflated = inflate(parent, viewType)
        return inflated.binding
    }

    override fun bind(binding: ViewDataBinding, note: Note, position: Int) {
        with(binding as LayoutNoteInfoBinding) {
            this.note = note
        }
    }

    private fun inflate(parent: ViewGroup, viewType: Int): InflationResult {
        val inflater = LayoutInflater.from(parent.context)
        val binding: LayoutNoteInfoBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.layout_note_info,
                parent,
                false,
                dataBindingComponent
        )
        return InflationResult(binding, binding.note)
    }

    private data class InflationResult(val binding: ViewDataBinding, val item: Note?)
}