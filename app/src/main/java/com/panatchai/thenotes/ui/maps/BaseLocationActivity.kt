package com.panatchai.thenotes.ui.maps

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.panatchai.thenotes.R
import com.panatchai.thenotes.ui.BaseActivity
import com.panatchai.thenotes.ui.models.maps.LocationPermissionStatus
import com.panatchai.thenotes.ui.models.maps.PermissionRequest
import javax.inject.Inject

/**
 * Base Activity for easy access to location.
 */
abstract class BaseLocationActivity : BaseActivity() {
    @Inject
    internal lateinit var locationPermissionHelper: LocationPermissionHelper

    @Inject
    internal lateinit var locationHelper: LocationHelper

    private var alertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationPermissionHelper.getRequestingPermissionStatus().observe(this, Observer { permissionRequest ->
            if (permissionRequest.status == LocationPermissionStatus.RATIONALE) {
                showPermissionRationale(permissionRequest)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        alertDialog?.dismiss()
        alertDialog = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationHelper.onActivityResult(requestCode, resultCode)
    }

    private fun showPermissionRationale(permissionRequest: PermissionRequest) {
        alertDialog = AlertDialog.Builder(this)
            .setTitle(R.string.location_permission_title)
            .setMessage(R.string.location_permission_desc)
            .setCancelable(true)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                //Prompt the user once explanation has been shown
                locationPermissionHelper.requestPermission(this@BaseLocationActivity, permissionRequest)
            }
            .setOnCancelListener {
                locationPermissionHelper.denied(permissionRequest.requestCode)
            }
            .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionHelper.onRequestPermissionsResult(requestCode, grantResults)
    }
}