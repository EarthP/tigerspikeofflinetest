package com.panatchai.thenotes.ui.common

/**
 * Generic interface for the retry button.
 */
interface RetryCallback {
    fun retry()
}
