package com.panatchai.thenotes.ui.models.maps

import com.google.android.gms.maps.model.LatLng

fun MapLocation.map(): LatLng = LatLng(latitude, longitude)