package com.panatchai.thenotes.ui.maps

import androidx.databinding.DataBindingComponent
import com.panatchai.thenotes.di.scopes.PerActivity
import javax.inject.Inject

@PerActivity
class NoteDataBindingComponent @Inject constructor(
        private val noteBindingAdapter: NoteBindingAdapter
) : DataBindingComponent {
    override fun getNoteBindingAdapter() = noteBindingAdapter
}