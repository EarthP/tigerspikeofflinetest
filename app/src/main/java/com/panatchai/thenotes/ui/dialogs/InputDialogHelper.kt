package com.panatchai.thenotes.ui.dialogs

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.panatchai.thenotes.R
import com.panatchai.thenotes.databinding.NoteInputLayoutBinding
import com.panatchai.thenotes.di.scopes.PerActivity
import javax.inject.Inject

/**
 * Helper class to create input dialog.
 *
 * :D I was planning on more elegant solution but this will have to do for the given time is limited.
 */
@PerActivity
class InputDialogHelper @Inject constructor(
    private val context: Context,
    private val lifecycleOwner: LifecycleOwner
): LifecycleObserver {

    private var alertDialog: AlertDialog? = null

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        alertDialog?.dismiss()
        alertDialog = null
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    private fun inflateInputView(): NoteInputLayoutBinding {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return DataBindingUtil.inflate(inflater, R.layout.note_input_layout, null, false)
    }

    fun showInputDialog() = showInputDialog("")

    fun showInputDialog(note: String): LiveData<String> {
        val inputLive = MutableLiveData<String>()

        val inputBinding = inflateInputView()
        if (note.isNotEmpty()) {
            inputBinding.inputEditText.setText(note)
            inputBinding.inputEditText.setSelection(note.length)
        }

        val builder = AlertDialog.Builder(context)
        builder.setView(inputBinding.root)
        builder.setCancelable(true)
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            val content = inputBinding.inputEditText.text.toString().trim()
            inputLive.value = content
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.cancel()
            inputLive.value = ""
            alertDialog = null
        }
        builder.setOnCancelListener { dialog ->
            dialog.cancel()
            inputLive.value = ""
            alertDialog = null
        }
        alertDialog = builder.show()
        return inputLive
    }
}