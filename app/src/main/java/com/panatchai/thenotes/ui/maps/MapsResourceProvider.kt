package com.panatchai.thenotes.ui.maps

import android.app.Application
import com.panatchai.thenotes.R
import com.panatchai.thenotes.utils.ResourceProvider
import javax.inject.Inject

/**
 * Specifically provide resources for MapsActivity and related classes.
 */
class MapsResourceProvider @Inject constructor(
        app: Application
) : ResourceProvider(app.applicationContext) {

    val cannotEditNoteError: String = getString(R.string.maps_activity_cannot_edit)
    val noteNotFound: String = getString(R.string.maps_activity_note_not_found)
}