package com.panatchai.thenotes.ui.models.maps

data class MapLocation(
    val latitude: Double,
    val longitude: Double
)