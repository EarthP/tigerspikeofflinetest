package com.panatchai.thenotes.ui.models.maps

/**
 * Holds a value with its permission request status.
 *
 * @param status [LocationPermissionStatus]
 * @param requestCode permission request code
 * @param permission Android Permission
 */
data class PermissionRequest(
    val status: LocationPermissionStatus,
    val requestCode: Int,
    val permission: String
) {
    companion object {
        fun granted(requestCode: Int): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.GRANTED, requestCode, "")
        }

        fun granted(requestCode: Int, permission: String): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.GRANTED, requestCode, permission)
        }

        fun denied(requestCode: Int): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.DENIED, requestCode, "")
        }

        fun denied(requestCode: Int, permission: String): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.DENIED, requestCode, permission)
        }

        fun rationale(requestCode: Int, permission: String): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.RATIONALE, requestCode, permission)
        }

        fun request(requestCode: Int, permission: String): PermissionRequest {
            return PermissionRequest(LocationPermissionStatus.REQUEST, requestCode, permission)
        }
    }
}