package com.panatchai.thenotes.ui.maps

import android.os.Bundle
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.snackbar.Snackbar
import com.panatchai.thenotes.R
import com.panatchai.thenotes.data.models.Status
import com.panatchai.thenotes.databinding.ActivityMapsBinding
import com.panatchai.thenotes.extensions.updateNote
import com.panatchai.thenotes.ui.common.RetryCallback
import com.panatchai.thenotes.ui.dialogs.InputDialogHelper
import com.panatchai.thenotes.ui.models.maps.LocationType
import javax.inject.Inject

class MapsActivity : BaseLocationActivity(), RetryCallback, SearchView.OnQueryTextListener {

    @Inject
    internal lateinit var mapHelper: MapsHelper

    @Inject
    internal lateinit var inputDialog: InputDialogHelper

    @Inject
    internal lateinit var noteListAdapter: NoteListAdapter

    private lateinit var viewModel: MapsViewModel
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        initView()
        initMap()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.isMapReady.removeObservers(this)
        viewModel.userLocation.removeObservers(this)
        viewModel.isAddNoteAllowed.removeObservers(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.queryText(newText ?: "")
        return false
    }

    override fun retry() {
        // TODO: Support Retry
    }

    private fun initViewModel() {
        viewModel = getViewModel(MapsViewModel::class.java)
        viewModel.isMapReady.observe(this, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let { noteList ->
                        mapHelper.clear()
                        for (note in noteList) {
                            mapHelper.addMarker(
                                    note.id,
                                    note.userName,
                                    note.note,
                                    note.latitude,
                                    note.longitude
                            )
                        }

                        noteListAdapter.submitList(noteList)
                    }

                    locationPermissionHelper.runIfHasPermission {
                        mapHelper.showMyLocation(true)
                    }

                    binding.noteList = viewModel.getCurrentNoteList()
                }
                Status.ERROR -> {
                    displayMessage(resource.message)
                }
                Status.LOADING -> {
                    binding.noteList = viewModel.getCurrentNoteList()
                }
            }
        })

        val title = getString(R.string.maps_activity_my_location)
        viewModel.userLocation.observe(this, Observer { location ->
            mapHelper.addMarker(
                    MAP_USER_ID,
                    title,
                    "",
                    location,
                    LocationType.USER
            )
        })
    }

    private fun initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        binding.lifecycleOwner = this
        binding.isAddNoteAllowed = viewModel.isAddNoteAllowed
        binding.callback = this
        binding.includeBottomSheet.noteList.adapter = noteListAdapter
        binding.mapsAddFab.setOnClickListener {
            inputDialog.showInputDialog().observe(this, Observer { inputText ->
                if (inputText.isNotEmpty()) {
                    viewModel.addNote(inputText)
                }
            })
        }
        binding.includeBottomSheet.mapsNoteSearch.setOnQueryTextListener(this)
    }

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(mapHelper)
        mapHelper.mapLoadStatus.observe(this, Observer { googleMap ->
            viewModel.setMapReady(googleMap != null)
        })

        // handle when updating note
        mapHelper.onMarkerEdit.observe(this, Observer { noteId ->
            if (noteId != MAP_USER_ID) {
                viewModel.editNote(noteId).observe(this, Observer { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let { foundNote ->
                                inputDialog.showInputDialog(foundNote.note).observe(this, Observer { inputText ->
                                    if (inputText.isNotEmpty()) {
                                        viewModel.updateNote(foundNote.updateNote(inputText))
                                    }
                                })
                            }
                        }
                        Status.ERROR -> {
                            displayMessage(resource.message)
                        }
                        Status.LOADING -> {
                            // no-opt
                        }
                    }
                })
            }
        })

        // Listen to location updates
        locationHelper.getUpdateLocation().observe(this, Observer { resource ->
            viewModel.updateUserLocation(resource)
        })
    }

    private fun displayMessage(msg: String) {
        Snackbar.make(binding.root, msg, Snackbar.LENGTH_SHORT).show()
    }

    companion object {
        private const val MAP_USER_ID = "0987654321234567890"
    }
}