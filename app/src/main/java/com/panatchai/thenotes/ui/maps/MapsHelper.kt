package com.panatchai.thenotes.ui.maps

import android.annotation.SuppressLint
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.models.maps.LocationType
import com.panatchai.thenotes.ui.models.maps.MapLocation
import com.panatchai.thenotes.ui.models.maps.map
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Facilitate adding, removing, editing Markers on the Maps.
 *
 * @param lifecycleOwner the lifecycle owner, which implement [LifecycleOwner]
 */
@PerActivity
class MapsHelper @Inject constructor(
        private val lifecycleOwner: LifecycleOwner,
        private val mapInfoAdapter: GoogleMapInFoWindowAdapter
) : LifecycleObserver, OnMapReadyCallback {

    // map of all markers
    private val markers = mutableMapOf<String, Marker>()
    private val markerInfo = mutableMapOf<String, MarkerInfo>()

    // weak ref to Maps
    private var mapRef: WeakReference<GoogleMap>? = null

    // notify the status of loading GoogleMap
    val mapLoadStatus: LiveData<GoogleMap> = MutableLiveData()

    // notify if the editing marker
    val onMarkerEdit: LiveData<String> = MutableLiveData<String>()

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        markers.clear()
        markerInfo.clear()
        mapRef?.clear()
        mapInfoAdapter.updateMarkersInfo(markerInfo)
        mapLoadStatus.removeObservers(lifecycleOwner)
        onMarkerEdit.removeObservers(lifecycleOwner)
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    /**
     * Add/Update a marker.
     *
     * @param id marker id
     * @param title marker title
     * @param content marker content
     * @param lat latitude
     * @param lng longitude
     */
    fun addMarker(
            id: String,
            title: String,
            content: String,
            lat: Double,
            lng: Double,
            locationType: LocationType = LocationType.LOCATION
    ) {
        addMarker(id, title, content, MapLocation(lat, lng), locationType)
    }

    /**
     * Add/Update a marker.
     *
     * @param id marker id
     * @param title marker title
     * @param content marker content
     * @param location [MapLocation]
     */
    fun addMarker(
            id: String,
            title: String,
            content: String,
            location: MapLocation,
            locationType: LocationType = LocationType.LOCATION
    ) {
        val latLng = location.map()
        // Add a marker and move the camera
        mapRef?.get()?.let { googleMap ->
            var marker = markers[id]
            if (marker != null) {
                // Update the marker
                marker.title = title
                marker.position = latLng
            } else {
                val option = MarkerOptions().position(latLng).title(title)

                // Changing marker icon if it is a user
                if (locationType == LocationType.USER) {
                    option.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation))
                }

                // Add a new marker
                marker = googleMap.addMarker(option)
                markers[id] = marker
            }
            marker?.let {
                markerInfo[it.id] = MarkerInfo(id, content)
            }
            mapInfoAdapter.updateMarkersInfo(markerInfo)
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        }
    }

    fun clear() {
        markers.clear()
        markerInfo.clear()
        mapRef?.get()?.clear()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mapRef = WeakReference(googleMap)
        googleMap.setInfoWindowAdapter(mapInfoAdapter)
        googleMap.setOnInfoWindowClickListener { mrk ->
            markerInfo[mrk.id]?.let { markerInfo ->
                (onMarkerEdit as MutableLiveData).value = markerInfo.id
            }
        }

        googleMap.uiSettings.isMapToolbarEnabled = false
        (mapLoadStatus as MutableLiveData).value = googleMap
    }

    /**
     * Show the current location button on Maps
     *
     * @param show True to show my current location icon. False to hide it.
     */
    @SuppressLint("MissingPermission")
    fun showMyLocation(show: Boolean) {
        mapRef?.get()?.isMyLocationEnabled = show
    }

    data class MarkerInfo(val id: String, val content: String)
}