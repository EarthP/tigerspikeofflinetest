package com.panatchai.thenotes.ui.maps

import androidx.recyclerview.widget.DiffUtil
import com.panatchai.thenotes.ui.models.notes.Note
import javax.inject.Inject

class NoteItemDiffCallback @Inject constructor() : DiffUtil.ItemCallback<Note>() {
    override fun areItemsTheSame(p0: Note, p1: Note): Boolean {
        return p0.id == p1.id
    }

    override fun areContentsTheSame(p0: Note, p1: Note): Boolean {
        return p0 == p1
    }
}