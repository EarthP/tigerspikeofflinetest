package com.panatchai.thenotes.ui.maps

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.panatchai.thenotes.R
import com.panatchai.thenotes.databinding.MapsInfoContentBinding
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.domain.UserUseCase
import javax.inject.Inject

@PerActivity
class GoogleMapInFoWindowAdapter @Inject constructor(
        private val context: Context,
        private val userUseCase: UserUseCase
): GoogleMap.InfoWindowAdapter {
    private lateinit var markerInfo: Map<String, MapsHelper.MarkerInfo>

    fun updateMarkersInfo(markerInfo: Map<String, MapsHelper.MarkerInfo>) {
        this.markerInfo = markerInfo
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }

    override fun getInfoContents(marker: Marker?): View {
        val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: MapsInfoContentBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.maps_info_content,
                null,
                false
        )

        marker?.let { mrk ->
            binding.mapsInfoUserText.text = mrk.title
            val content = markerInfo[mrk.id]?.content
            if (content != null && content.isNotEmpty()) {
                binding.mapsInfoContentText.text = markerInfo[mrk.id]?.content
                if (userUseCase.getCurrentUserName() != mrk.title) {
                    binding.mapsInfoEditText.visibility = View.GONE
                }
            } else {
                binding.mapsInfoContentText.visibility = View.GONE
                binding.mapsInfoEditText.visibility = View.GONE
            }
        }

        return binding.root
    }
}