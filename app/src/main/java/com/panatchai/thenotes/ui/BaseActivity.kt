package com.panatchai.thenotes.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Base Activity that provide easy access to ViewModel. Also Dagger.
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    internal lateinit var vmFactory: ViewModelProvider.Factory

    protected fun <T : ViewModel> getViewModel(clazz: Class<T>): T = ViewModelProviders.of(this, vmFactory).get(clazz)
}