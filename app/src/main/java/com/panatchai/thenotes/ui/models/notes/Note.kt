package com.panatchai.thenotes.ui.models.notes

data class Note(
    val userId: Long,
    val userName: String,
    val latitude: Double,
    val longitude: Double,
    val createdDate: Long,
    val modifiedDate: Long = 0,
    val note: String
) {
    val title = if (note.isBlank()) {
        UNTITLE
    } else {
        val length = if (note.length < TITLE_LENGTH) {
            note.length
        } else {
            TITLE_LENGTH
        }
        note.substring(0, length)
    }

    val id = "$userName#$userId"

    companion object {
        private const val UNTITLE = "No Title"
        private const val TITLE_LENGTH = 10
    }
}