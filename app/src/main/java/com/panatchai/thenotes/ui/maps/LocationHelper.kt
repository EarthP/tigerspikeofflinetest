package com.panatchai.thenotes.ui.maps

import android.annotation.SuppressLint
import android.app.Activity
import android.content.IntentSender
import android.location.Location
import android.os.Looper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.Task
import com.panatchai.thenotes.data.models.Resource
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.models.maps.LocationPermissionStatus
import com.panatchai.thenotes.ui.models.maps.MapLocation
import javax.inject.Inject

/**
 * Help getting locations.
 *
 * @param activity host activity
 * @param lifecycleOwner the lifecycle owner, which implement [LifecycleOwner]
 * @param permissionHelper [LocationPermissionHelper]
 */
@PerActivity
class LocationHelper @Inject constructor(
        private val activity: Activity,
        private val lifecycleOwner: LifecycleOwner,
        private val permissionHelper: LocationPermissionHelper
) : LifecycleObserver, LocationCallback() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // will be notified when there is a location update.
    private val newLocation = MutableLiveData<Resource<MapLocation>>()

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    internal fun onCreate() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    internal fun onStart() {
        createLocationRequest()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    internal fun onStop() {
        fusedLocationClient.removeLocationUpdates(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun onDestroy() {
        newLocation.removeObservers(lifecycleOwner)
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    override fun onLocationResult(result: LocationResult?) {
        super.onLocationResult(result)
        result?.let { location ->
            newLocation.value = Resource.success(
                    MapLocation(
                            latitude = location.lastLocation.latitude,
                            longitude = location.lastLocation.longitude
                    )
            )
        }
    }

    /**
     * Create location update and also ask user to turn on the location service.
     */
    @SuppressLint("MissingPermission")
    private fun createLocationRequest() {
        LocationRequest.create()?.apply {
            interval = UPDATE_INTERVAL
            fastestInterval = FAST_UPDATE_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY

            newLocation.value = Resource.loading(data = null)
            // Try turning on the location service if it is disabled.
            val builder = LocationSettingsRequest.Builder()
                    .setAlwaysShow(true) // required the location service to be turned on.
                    .addLocationRequest(this)

            val client: SettingsClient = LocationServices.getSettingsClient(activity)
            val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

            // If the location service is enabled.
            task.addOnSuccessListener {
                // Start listening to the location updates.
                permissionHelper.getRequestingPermissionStatus().observe(lifecycleOwner, Observer { permissionRequest ->
                    if (permissionRequest.status == LocationPermissionStatus.GRANTED) {
                        // Getting the last known location first.
                        fusedLocationClient.lastLocation
                                .addOnSuccessListener { location: Location? ->
                                    location?.let { lastLocation ->
                                        newLocation.value = Resource.loading(
                                                MapLocation(
                                                        latitude = lastLocation.latitude,
                                                        longitude = lastLocation.longitude
                                                )
                                        )
                                    }
                                }
                        // Requesting for more accurate updates
                        fusedLocationClient.requestLocationUpdates(this, this@LocationHelper, Looper.myLooper())
                    } else {
                        newLocation.value = Resource.error(data = null)
                    }
                })
            }

            // If the location service is disabled still.
            task.addOnFailureListener { exception ->
                if (exception is ResolvableApiException
                        && !activity.isDestroyed
                        && !activity.isFinishing) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.

                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        exception.startResolutionForResult(
                                activity,
                                REQUEST_CHECK_SETTINGS
                        )
                    } catch (sendEx: IntentSender.SendIntentException) {
                        // Ignore the error.
                        newLocation.value = Resource.error(
                                sendEx.message ?: sendEx.localizedMessage,
                                null
                        )
                    }
                } else {
                    newLocation.value = Resource.error(data = null)
                }
            }
        }
    }

    /**
     * When there is a new location update, this will be notified.
     */
    fun getUpdateLocation(): LiveData<Resource<MapLocation>> = newLocation

    fun onActivityResult(requestCode: Int, resultCode: Int) {
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        createLocationRequest()
                    }
                    Activity.RESULT_CANCELED -> {
                        newLocation.value = Resource.error(data = null)
                    }
                }
            }
        }
    }

    companion object {
        private const val UPDATE_INTERVAL = 20000L
        private const val FAST_UPDATE_INTERVAL = 10000L
        private const val REQUEST_CHECK_SETTINGS = 39210
    }
}