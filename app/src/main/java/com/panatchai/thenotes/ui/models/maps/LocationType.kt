package com.panatchai.thenotes.ui.models.maps

/**
 * Type of location markers
 */
enum class LocationType {
    USER,
    LOCATION
}
