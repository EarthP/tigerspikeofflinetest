package com.panatchai.thenotes.ui.maps

import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.get
import androidx.databinding.BindingAdapter
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.models.notes.Note
import javax.inject.Inject

@PerActivity
class NoteBindingAdapter @Inject constructor() {

    @BindingAdapter("note")
    fun bindNote(viewGroup: ViewGroup, note: Note) {
        if (viewGroup.childCount < 2) {
            return
        }

        val titleText = viewGroup[0] as TextView
        val noteText = viewGroup[1] as TextView
        titleText.text = note.userName
        noteText.text = note.note
    }
}