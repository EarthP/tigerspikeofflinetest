package com.panatchai.thenotes.ui.models.maps

/**
 * Status of Requesting Permission.
 */
enum class LocationPermissionStatus {
    GRANTED,
    DENIED,
    RATIONALE,
    REQUEST
}