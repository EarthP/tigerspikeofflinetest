package com.panatchai.thenotes.ui.binding

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {
    @JvmStatic
    @BindingAdapter("isVisible")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("formatDate")
    fun formatDate(textView: TextView, milliSec: Long) {
        val currentLocale = textView.context.resources.configuration.locales.get(0)
        val displayDf = SimpleDateFormat("MMM d, yyyy h:mm a", currentLocale)

        val calendar = Calendar.getInstance(currentLocale)
        calendar.timeInMillis = milliSec
        textView.text = try {
            displayDf.format(calendar.time)
        } catch (e: Exception) {
            ""
        }
    }
}
