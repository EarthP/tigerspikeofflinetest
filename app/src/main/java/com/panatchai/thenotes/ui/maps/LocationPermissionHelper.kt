package com.panatchai.thenotes.ui.maps

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.lifecycle.*
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.models.maps.LocationPermissionStatus
import com.panatchai.thenotes.utils.BasePermissionHelper
import javax.inject.Inject

/**
 * Specifically facilitate requesting location permission.
 *
 * @param activity owner [Activity]
 * @param lifecycleOwner the lifecycle owner, which implement [LifecycleOwner]
 */
@PerActivity
class LocationPermissionHelper @Inject constructor(
    private val activity: Activity,
    private val lifecycleOwner: LifecycleOwner
) : BasePermissionHelper(), LifecycleObserver {

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    internal fun onCreate() {
        checkPermission(activity, PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun onDestroy() {
        lifecycleOwner.lifecycle.removeObserver(this)
    }

    /**
     * Handle [Activity.onRequestPermissionsResult].
     */
    fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    granted(requestCode)
                } else {
                    denied(requestCode)
                }
            }
        }
    }

    /**
     * Run the given block of code if permission is granted.
     *
     * @param onPermissionGranted code block to be executed.
     */
    fun runIfHasPermission(onPermissionGranted: () -> Unit) {
        getRequestingPermissionStatus().observe(lifecycleOwner, Observer { permissionRequest ->
            if (permissionRequest.status == LocationPermissionStatus.GRANTED) {
                onPermissionGranted()
            }
        })
    }

    companion object {
        private const val PERMISSIONS_REQUEST_LOCATION = 999
    }
}