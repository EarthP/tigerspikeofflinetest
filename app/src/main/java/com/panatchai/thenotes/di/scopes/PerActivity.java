package com.panatchai.thenotes.di.scopes;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A custom scope stating the life-cycle of the annotated dependency that is Singleton to Activity.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}