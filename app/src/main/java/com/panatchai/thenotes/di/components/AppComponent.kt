package com.panatchai.thenotes.di.components

import android.app.Application
import com.panatchai.thenotes.application.TheNoteApp
import com.panatchai.thenotes.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * This holds the dependency graph (what depends on what) of the Application
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class, // <-- Create ViewModel & Provider
        ActivityContributorModule::class, // <-- Activity-Sub-Graphs
        FragmentContributorModule::class,  // <-- Fragment-Sub-Graphs
        ApiModule::class, // <-- Instantiate api related dependencies
        BindingModule::class // <-- Bind Abstract/Interface with Concrete
    ]
)
interface AppComponent : AndroidInjector<TheNoteApp> {
    // Exposing list of dependencies goes here.
    fun application(): Application

    // List of injection point goes here if any.

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}