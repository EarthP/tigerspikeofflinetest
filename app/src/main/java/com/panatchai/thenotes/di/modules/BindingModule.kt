package com.panatchai.thenotes.di.modules

import android.app.Application
import android.content.Context
import com.panatchai.thenotes.data.repositories.NoteRepository
import com.panatchai.thenotes.data.repositories.NoteRepositoryImp
import com.panatchai.thenotes.di.scopes.ForApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class BindingModule {
    @Binds
    @Singleton
    abstract fun bindNoteRepository(noteRepository: NoteRepositoryImp): NoteRepository

    @Binds
    @Singleton
    @ForApplication
    abstract fun bindAppContext(app: Application): Context
}