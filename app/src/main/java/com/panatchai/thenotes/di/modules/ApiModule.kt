package com.panatchai.thenotes.di.modules

import dagger.Module

/**
 * Hold the key of how Network-Api-Dependencies are createdDate (if any).
 */
@Module
class ApiModule