package com.panatchai.thenotes.di.modules

import dagger.Module

/**
 * Hold the key of how dependencies are createdDate. Also contain Fragment-Sub-Graph declarations.
 */
@Module
abstract class FragmentContributorModule