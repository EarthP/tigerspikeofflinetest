package com.panatchai.thenotes.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.panatchai.thenotes.di.scopes.ViewModelKey
import com.panatchai.thenotes.ui.maps.MapsViewModel
import com.panatchai.thenotes.utils.TheNoteViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MapsViewModel::class)
    abstract fun bindMapsViewModel(mapsViewModel: MapsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: TheNoteViewModelFactory): ViewModelProvider.Factory
}