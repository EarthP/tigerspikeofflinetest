package com.panatchai.thenotes.di.modules

import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.maps.MapsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Hold the key of how dependencies are createdDate. Also contain Activity-Sub-Graph declarations.
 */
@Module
abstract class ActivityContributorModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [MapsActivityModule::class])
    abstract fun contributeMapsActivity(): MapsActivity
}