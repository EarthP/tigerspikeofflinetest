package com.panatchai.thenotes.di.modules

import android.app.Activity
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.panatchai.thenotes.di.scopes.PerActivity
import com.panatchai.thenotes.ui.maps.MapsActivity
import dagger.Binds
import dagger.Module

@Module
abstract class MapsActivityModule {

    @Binds
    @PerActivity
    abstract fun bindActivity(activity: MapsActivity): Activity

    @Binds
    @PerActivity
    abstract fun bindActivityContext(activity: Activity): Context

    @Binds
    @PerActivity
    abstract fun bindLifeCycleOwner(activity: MapsActivity): LifecycleOwner
}