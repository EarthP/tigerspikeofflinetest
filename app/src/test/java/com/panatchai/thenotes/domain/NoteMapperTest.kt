package com.panatchai.thenotes.domain

import com.panatchai.thenotes.data.models.notes.RawNote
import com.panatchai.thenotes.ui.models.notes.Note
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

@Suppress("TestFunctionName")
class NoteMapperTest {

    @Test
    fun GivenRawNote_WhenMap_ReturnNote() {
        val actual = rawNote.map()
        assertThat(actual).isEqualTo(note)
    }

    @Test
    fun GivenNote_WhenMap_ReturnRawNote() {
        val actual = note.map()
        assertThat(actual).isEqualTo(rawNote)
    }

    @Test
    fun GivenListOfRawNote_WhenMap_ReturnListOfNote() {
        val rawList = listOf(rawNote)

        val actual = rawList.map()
        val expected = listOf(note)

        assertThat(actual).isEqualTo(expected)
    }

    companion object {
        private val note = Note(
            userId = 999999,
            userName = "name",
            latitude = 13.0,
            longitude = 100.0,
            createdDate = 1234567890,
            modifiedDate = 987654321,
            note = "some note"
        )

        val rawNote = RawNote(
            uid = 999999,
            uname = "name",
            lat = 13.0,
            lng = 100.0,
            created = 1234567890,
            modified = 987654321,
            note = "some note"
        )
    }
}