package com.panatchai.thenotes.domain

import com.nhaarman.mockitokotlin2.*
import com.panatchai.thenotes.data.repositories.UserSharedPrefs
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

@Suppress("TestFunctionName")
class UserUseCaseTest {
    private val userSharedPrefs: UserSharedPrefs = mock()

    @Test
    fun GivenExistingUser_WhenGetUserId_ReturnExistingUserId() {
        // init mocks
        whenever(userSharedPrefs.getUserId()).thenReturn(999)
        val userUseCase = UserUseCase(userSharedPrefs)

        // execute test
        val userId = userUseCase.getCurrentUserId()

        // verify
        verify(userSharedPrefs, times(1)).getUserId()
        verify(userSharedPrefs, never()).saveUserId(userId)
    }

    @Test
    fun GivenNoUser_WhenGetUserId_ReturnNewUserId() {
        // init mocks
        whenever(userSharedPrefs.getUserId()).thenReturn(0)
        val userUseCase = UserUseCase(userSharedPrefs)

        // execute test
        val userId = userUseCase.getCurrentUserId()

        // verify
        verify(userSharedPrefs, times(1)).getUserId()
        verify(userSharedPrefs, times(1)).saveUserId(userId)
        assertThat(userId).isNotEqualTo(0)
    }

    @Test
    fun GivenExistingUser_WhenGetUserName_ReturnExistingUserName() {
        // init mocks
        whenever(userSharedPrefs.getUserName()).thenReturn("name")
        val userUseCase = UserUseCase(userSharedPrefs)

        // execute test
        val userName = userUseCase.getCurrentUserName()

        // verify
        verify(userSharedPrefs, times(1)).getUserName()
        verify(userSharedPrefs, never()).saveUseName(userName)
    }

    @Test
    fun GivenNoUser_WhenGetUserName_ReturnNewUserName() {
        // init mocks
        whenever(userSharedPrefs.getUserName()).thenReturn("")
        val userUseCase = UserUseCase(userSharedPrefs)

        // execute test
        val userName = userUseCase.getCurrentUserName()

        // verify
        verify(userSharedPrefs, times(1)).getUserName()
        verify(userSharedPrefs, times(1)).saveUseName(userName)
        assertThat(userName).isNotEmpty()
    }
}