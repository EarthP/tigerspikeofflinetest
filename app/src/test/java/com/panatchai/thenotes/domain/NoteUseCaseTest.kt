package com.panatchai.thenotes.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import com.panatchai.thenotes.data.models.Resource
import com.panatchai.thenotes.data.models.Status
import com.panatchai.thenotes.data.models.notes.RawNote
import com.panatchai.thenotes.data.repositories.NoteRepository
import com.panatchai.thenotes.ui.models.maps.MapLocation
import com.panatchai.thenotes.ui.models.notes.Note
import com.panatchai.thenotes.utils.AppExecutors
import com.panatchai.thenotes.utils.InstantAppExecutors
import com.panatchai.thenotes.utils.TimeUtil
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@Suppress("TestFunctionName")
class NoteUseCaseTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val noteRepository: NoteRepository = mock()
    private val userUseCase: UserUseCase = mock()
    private val timeUtil: TimeUtil = mock()
    private val appExecutors: AppExecutors = InstantAppExecutors()

    private lateinit var noteUseCase: NoteUseCase

    @Before
    fun init() {
        whenever(userUseCase.getCurrentUserId()).thenReturn(11)
        whenever(userUseCase.getCurrentUserName()).thenReturn("Earth")
        whenever(timeUtil.getCurrentTimeLong()).thenReturn(1111)

        noteUseCase = NoteUseCase(
            noteRepository,
            userUseCase,
            timeUtil,
            appExecutors
        )
    }

    @Test
    fun WhenGetNotes_ReturnListOfNotes() {
        // init mocks
        val rawList = MutableLiveData<Resource<List<RawNote>>>()
        whenever(noteRepository.getNotes()).thenReturn(rawList)

        // execute test
        val liveData = noteUseCase.getNotes()
        val observer: Observer<Resource<List<Note>>> = mock()
        liveData.observeForever(observer)

        // verify
        rawList.value = Resource.loading(listOf(RAW_NOTE))
        val captor = argumentCaptor<Resource<List<Note>>>()
        verify(observer, times(1)).onChanged(captor.capture())
        var expected = Resource.loading(listOf(UI_NOTE))
        assertThat(captor.lastValue).isEqualToComparingFieldByField(expected)
        assertThat(captor.lastValue.data).isEqualTo(listOf(UI_NOTE))

        reset(observer)
        rawList.value = Resource.success(listOf(RAW_NOTE))
        verify(observer, times(1)).onChanged(captor.capture())
        expected = Resource.success(listOf(UI_NOTE))
        assertThat(captor.lastValue).isEqualToComparingFieldByField(expected)
        assertThat(captor.lastValue.data).isEqualTo(listOf(UI_NOTE))

        reset(observer)
        rawList.value = Resource.error("ERROR", listOf(RAW_NOTE))
        verify(observer, times(1)).onChanged(captor.capture())
        expected = Resource.error("ERROR", listOf(UI_NOTE))
        assertThat(captor.lastValue).isEqualToComparingFieldByField(expected)
        assertThat(captor.lastValue.data).isEqualTo(listOf(UI_NOTE))
        assertThat(captor.lastValue.message).isEqualTo(expected.message)

        // clean up
        liveData.removeObserver(observer)
    }

    @Test
    fun WhenAddNote_AddNewNote() {
        // execute test
        val text = "ABCD"
        val location = MapLocation(11.0, 99.0)
        noteUseCase.addNote(text, location)

        // verify
        val expected = RawNote(
            uid = userUseCase.getCurrentUserId(),
            uname = userUseCase.getCurrentUserName(),
            lat = location.latitude,
            lng = location.longitude,
            created = timeUtil.getCurrentTimeLong(),
            modified = 0,
            note = text
        )
        val captor = argumentCaptor<RawNote>()
        verify(noteRepository, times(1)).addNote(captor.capture())
        assertThat(captor.firstValue).isEqualToComparingFieldByField(expected)
    }

    @Test
    fun GivenNoteIsEditable_ThenReturnTrue() {
        // init mocks
        whenever(userUseCase.getCurrentUserId()).thenReturn(UI_NOTE.userId)

        // execute test
        val actual = noteUseCase.canEditNote(UI_NOTE)

        // verify
        assertThat(actual).isTrue()
    }

    @Test
    fun GivenNoteIsNotEditable_ThenReturnFalse() {
        // init mocks
        whenever(userUseCase.getCurrentUserId()).thenReturn(9806987)

        // execute test
        val actual = noteUseCase.canEditNote(UI_NOTE)

        // verify
        assertThat(actual).isFalse()
    }

    @Test
    fun GivenEditable_WhenUpdateNote_UpdateNote() {
        // init mocks
        whenever(userUseCase.getCurrentUserId()).thenReturn(UI_NOTE.userId)

        // execute test
        noteUseCase.updateNote(UI_NOTE)

        // verify
        val captor = argumentCaptor<RawNote>()
        verify(noteRepository, times(1)).updateNote(captor.capture())
        assertThat(captor.firstValue).isEqualToComparingFieldByField(RAW_NOTE)
    }

    @Test
    fun GivenNotEditable_WhenUpdateNote_DoNothing() {
        // init mocks
        whenever(userUseCase.getCurrentUserId()).thenReturn(984793843)

        // execute test
        noteUseCase.updateNote(UI_NOTE)

        // verify
        verify(noteRepository, never()).updateNote(any())
    }

    @Test
    fun GivenMatchedQuery_ReturnListOfNotes() {
        // init mocks
        val expected = MutableLiveData<Resource<List<RawNote>>>()
        whenever(noteRepository.getNotes()).thenReturn(expected)

        // execute test
        val queryResult = noteUseCase.queryNote("some")

        // verify
        val observer: Observer<Resource<List<Note>>> = mock()
        val captor = argumentCaptor<Resource<List<Note>>>()
        queryResult.observeForever(observer)
        expected.value = Resource.success(listOf(RAW_NOTE))
        verify(observer, times(1)).onChanged(captor.capture())
        assertThat(captor.firstValue).isEqualToComparingFieldByField(Resource.success(listOf(UI_NOTE)))
        assertThat(captor.firstValue.data).isEqualTo(listOf(UI_NOTE))
        assertThat(captor.firstValue.data?.get(0)).isNotNull
        assertThat(captor.firstValue.data?.get(0)).isEqualTo(UI_NOTE)

        // clean up
        queryResult.removeObserver(observer)
    }

    @Test
    fun GivenNotMatchedQuery_ReturnError() {
        // init mocks
        val expected = MutableLiveData<Resource<List<RawNote>>>()
        whenever(noteRepository.getNotes()).thenReturn(expected)

        // execute test
        var queryResult = noteUseCase.queryNote("xxxxx")

        // verify
        val observer: Observer<Resource<List<Note>>> = mock()
        val captor = argumentCaptor<Resource<List<Note>>>()
        queryResult.observeForever(observer)
        expected.value = Resource.success(listOf())
        verify(observer, times(1)).onChanged(captor.capture())
        assertThat(captor.lastValue.status).isEqualTo(Status.ERROR)
        assertThat(captor.lastValue.message).isEqualTo("xxxxx Not Found!")
        // clean up
        queryResult.removeObserver(observer)

        reset(observer)
        expected.value = Resource.error("ERROR")
        queryResult = noteUseCase.queryNote("xxxxx")
        queryResult.observeForever(observer)
        verify(observer, times(1)).onChanged(captor.capture())
        assertThat(captor.lastValue.status).isEqualTo(Status.ERROR)
        assertThat(captor.lastValue.message).isEqualTo("ERROR")
        // clean up
        queryResult.removeObserver(observer)

        reset(observer)
        expected.value = Resource.loading(null)
        queryResult = noteUseCase.queryNote("xxxxx")
        queryResult.observeForever(observer)
        verify(observer, times(1)).onChanged(captor.capture())
        assertThat(captor.lastValue.status).isEqualTo(Status.LOADING)
        // clean up
        queryResult.removeObserver(observer)
    }

    companion object {
        private val UI_NOTE = Note(
            userId = 999999,
            userName = "name",
            latitude = 13.0,
            longitude = 100.0,
            createdDate = 1234567890,
            modifiedDate = 987654321,
            note = "some note"
        )

        val RAW_NOTE = RawNote(
            uid = 999999,
            uname = "name",
            lat = 13.0,
            lng = 100.0,
            created = 1234567890,
            modified = 987654321,
            note = "some note"
        )
    }
}