package com.panatchai.thenotes.data.repositories

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Suppress("TestFunctionName")
@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class UserSharedPrefsTest {

    @Test
    fun WhenGetUserId_ReturnUserId() {
        // init mocks
        val context = ApplicationProvider.getApplicationContext<Context>()
        val userSharedPrefs = UserSharedPrefs(context)

        // execute test
        userSharedPrefs.saveUserId(999)

        // verify
        assertThat(userSharedPrefs.getUserId()).isEqualTo(999)
    }

    @Test
    fun WhenGetUserName_ReturnUserName() {
        // init mocks
        val context = ApplicationProvider.getApplicationContext<Context>()
        val userSharedPrefs = UserSharedPrefs(context)

        // execute test
        userSharedPrefs.saveUseName("name")

        // verify
        assertThat(userSharedPrefs.getUserName()).isEqualTo("name")
    }

    @Test
    fun WhenSaveUserId_SaveUserId() {
        // init mocks
        val context = ApplicationProvider.getApplicationContext<Context>()
        val sharedPrefs = context.getSharedPreferences(
            SharedPrefs.SHARED_PREFERENCES_NAME,
            Context.MODE_PRIVATE
        )
        val userSharedPrefs = UserSharedPrefs(context)

        // execute test
        userSharedPrefs.saveUserId(999)

        // verify
        assertThat(sharedPrefs.getLong(UserSharedPrefs.USER_ID, 0)).isEqualTo(999)
    }

    @Test
    fun WhenSaveUserName_SaveUserName() {
        // init mocks
        val context = ApplicationProvider.getApplicationContext<Context>()
        val sharedPrefs = context.getSharedPreferences(
            SharedPrefs.SHARED_PREFERENCES_NAME,
            Context.MODE_PRIVATE
        )
        val userSharedPrefs = UserSharedPrefs(context)

        // execute test
        userSharedPrefs.saveUseName("name")

        // verify
        assertThat(sharedPrefs.getString(UserSharedPrefs.USER_NAME, "")).isEqualTo("name")
    }
}