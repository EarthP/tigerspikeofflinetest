package com.panatchai.thenotes.utils

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.panatchai.thenotes.R
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Suppress("TestFunctionName")
@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class ResourceProviderTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    @Test
    fun testGetString() {
        // init mocks
        val resourceProvider = ResourceProvider(context)

        // execute test
        val actual = resourceProvider.getString(R.string.app_name)
        assertThat(actual).isEqualTo("TheNotes")

        // clean up
    }

    @Test
    fun GivenParam_WhenGetString_ReturnStringWithParam() {
        // init mocks
        val resourceProvider = ResourceProvider(context)

        // execute test
        val actual = resourceProvider.getString(R.string.test_format, "1")
        assertThat(actual).isEqualTo("Test 1")

        // clean up
    }
}