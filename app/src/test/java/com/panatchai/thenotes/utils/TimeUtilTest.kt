package com.panatchai.thenotes.utils

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class TimeUtilTest {

    @Test
    fun testGetCurrentTimeLong() {
        // init mocks
        val app = ApplicationProvider.getApplicationContext<Application>()
        val timeUtil = TimeUtil(app)
        val locale = app.resources.configuration.locales.get(0)
        val expected = Calendar.getInstance(locale)

        // execute test
        val currentTime = timeUtil.getCurrentTimeLong()
        val expectedTime = expected.timeInMillis

        // There is no way to test this but this (seconds) should be close enough
        assertThat(currentTime / 1000).isEqualTo(expectedTime / 1000)

        // clean up
    }

}